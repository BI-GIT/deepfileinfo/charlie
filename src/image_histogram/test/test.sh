#!/usr/bin/env bash
cd "$(dirname "$0")" || exit
gcc -lm -o histogram ../image_histogram.c

for f in *.png *.jpg
do
  diff <(./histogram "$f") "$f.histogram"
done

rm histogram
